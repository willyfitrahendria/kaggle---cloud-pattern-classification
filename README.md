# Kaggle - Cloud Pattern Classification

Description
---
![](https://storage.googleapis.com/kaggle-media/competitions/MaxPlanck/Teaser_AnimationwLabels.gif)
> Climate change has been at the top of our minds and on the forefront of important political decision-making for many years. We hope you can use this competition’s dataset to help demystify an important climatic variable. Scientists, like those at Max Planck Institute for Meteorology, are leading the charge with new research on the world’s ever-changing atmosphere and they need your help to better understand the clouds.
> 
> Shallow clouds play a huge role in determining the Earth's climate. They’re also difficult to understand and to represent in climate models. By classifying different types of cloud organization, researchers at Max Planck hope to improve our physical understanding of these clouds, which in turn will help us build better climate models.
> 
> There are many ways in which clouds can organize, but the boundaries between different forms of organization are murky. This makes it challenging to build traditional rule-based algorithms to separate cloud features. The human eye, however, is really good at detecting features—such as clouds that resemble flowers.
> 
> In this challenge, you will build a model to classify cloud organization patterns from satellite images. If successful, you’ll help scientists to better understand how clouds will shape our future climate. This research will guide the development of next-generation models which could reduce uncertainties in climate projections.
> 
> Help us remove the haze from climate models and bring clarity to cloud identification.
> 
> For more information on the scientific background and how the labels were created see the following [paper](https://arxiv.org/abs/1906.01906).

Overview
---


*  **Model**

    vanilla unet

    encoder: resnet50 (imagenet pretrained)
    
*  **Train**

    train_ test_ split(train:90%, val:10%)
    
    augmentation: HorizontalFlip, ShiftScaleRotate, GridDistortion, OpticalDistortion
    
    loss: bce dice loss
    
    optimizer: adam

    epochs: 20

* **Test**

    best threshold (0, 100, 5) & best min size (0, 1000, 10000, 15000, 20000, 25000)
    
    small mask ignored
    
Result (Not Final)
---
Current score : 0.649

Current rank : bronze medal

In progress
---
...